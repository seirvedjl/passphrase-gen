#!/usr/bin/python
# Generate a secure (pseudo) random passphrase

import random, json, sys, argparse


"""load_words
load a dictionary of words from 'dict.json'"""
def load_words():
    with open('dict.json') as word_file:
        return json.load(word_file)


"""rand_word
generate a pseudo-random word"""
def rand_word(word_list):
    return random.choice(list(word_list))


"""rand_num
generate a pseudo-random number"""
def rand_num():
    return str(random.choice(range(0, 9)))


"""phrase_list
generate a phrase, returns a list of strings"""
# TODO: add functionality to append a pre-determined amount of numbers to phrase
# E.g. 1-4 numbers
def phrase_list( word_list, num_words, num_on):
    phrase = []

    i = 1
    while (i <= num_words):
        phrase.append(rand_word(word_list))
        i += 1
    # shuffle strings in list for added randomness
    # add extra string to sample if user selects num_on
    if num_on == True:
        phrase.append(rand_num())
        return random.sample(phrase, num_words + 1)
    else:
        return random.sample(phrase, num_words)


"""cli argument parser"""
def parse_args(args):
    parser = argparse.ArgumentParser(description='Generate secure pseudo-random passphrase')

    parser.add_argument("-n", "--num_on", action="store_true", default=False,
                        dest="num_on",
                        help="set word delimiter for phrase")

    parser.add_argument("-w", "--num_words", type=int, default=4,
                        dest="num_words",
                        help="set number of word (strings) in passphrase")

    parser.add_argument("-d", "--delimiter", default=" ",
                        dest="delimiter",
                        help="set word delimiter for phrase")

    return parser.parse_args()


"""main"""
def main(arguments):
    args = parse_args(arguments)
    words = load_words()
    num_words = args.num_words
    delimiter = args.delimiter
    num_on = args.num_on

    print(*phrase_list( words, num_words, num_on), sep = delimiter)


# entrypoint
if __name__ == '__main__':
    main(sys.argv[1:])
